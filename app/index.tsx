import * as React from "react";
import * as ReactDOM from "react-dom";
import {BrowserRouter, Route, Link} from "react-router-dom";

import {Main} from "./Main.tsx";
import Add from './Add.tsx';
import Lorem from './Lorem.tsx';

import './index.less';

class App extends React.Component {
    render() {

        return(
            <BrowserRouter>
                <div>
                    <div className="top"> CONNECTED </div>

                    <div className="menu">
                        <ul>
                            <li className = "menu_nav">    <Link to = "/"     > Main  </Link>    </li>
                            <li className = "menu_nav">    <Link to = "/add"  > Add   </Link>    </li>
                            <li className = "menu_nav">    <Link to = "/lorem"> Lorem </Link>    </li>
                        </ul>
                    </div>

                    <div>
                        <Route exact  path = "/"       component = {Main}  />
                        <Route        path = "/add"    component = {Add}   />
                        <Route        path = "/lorem"  component = {Lorem} />
                    </div>
                </div>   
            </BrowserRouter>
        )
    }
}

ReactDOM.render( <App />, document.getElementById("root") );
import * as React from 'react';

import "./Lorem.less"

export default class Lorem extends React.Component {
    render(){
        return(
	        <div className="wrapper">
	        	<p id="lorem">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At eaque, non ad libero doloremque, voluptas cum cupiditate minima, magnam reprehenderit adipisci. Tempora assumenda, eum expedita, accusantium ea animi. Omnis consectetur sunt magnam officiis, sint ea dolor ducimus, corporis, cumque iure culpa fuga ullam fugiat provident! Enim optio vel mollitia ducimus, autem qui incidunt. Incidunt, natus quaerat harum expedita omnis corporis necessitatibus, ipsum. Quidem libero necessitatibus ipsa eaque numquam, consectetur qui sit. Labore ex soluta voluptate dolores similique sapiente qui, amet, quisquam fugiat minus. Architecto eligendi ex culpa, sapiente vitae aliquam, excepturi officia nesciunt eaque et unde nemo nobis corporis quae, maxime voluptas sequi recusandae soluta earum adipisci fugit deserunt. A officia non, repellat labore neque iusto, dolorum molestiae blanditiis harum dolore error at soluta unde consequatur repellendus quaerat asperiores alias. Incidunt id vitae velit aspernatur sit esse voluptate eaque sequi cum illo, consequatur a minima, autem quo mollitia tempora deserunt modi odio doloremque quidem odit iure vel! Expedita aliquam similique doloribus itaque vero esse facere nostrum magni voluptate, repellendus asperiores dolor at animi sapiente, maiores, ullam fuga iure perferendis voluptatibus enim? Soluta fuga voluptates, officia obcaecati animi culpa aliquam odit aspernatur nobis qui modi repellat, dolorem cum minima, unde enim.</p>
	    	</div>
	    );
    }
}
import * as React from "react";

import './Main.less';

const my_tickets: IArticle[] = [
    {
        id: "iugikug",
        organization: 'Организация 1',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio cupiditate temporibus sunt. Accusantium obcaecati, exercitationem enim cupiditate placeat tempora. Quod.'
    },
    {
        id: "iugikugfgfd",
        organization: 'Организация 2',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio cupiditate temporibus sunt. Accusantium obcaecati, exercitationem enim cupiditate placeat tempora. Quod.'
    },
    {
        id: "iugidsafkug",
        organization: 'Организация 5',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio cupiditate temporibus sunt. Accusantium obcaecati, exercitationem enim cupiditate placeat tempora. Quod.'
    }
];

interface ITicketProps {
    data: IArticle[];
}

export class Ticket extends React.Component<ITicketProps, {}> {
    render () {
        const {data} = this.props;
        let ticketTemplate;

        if (data.length > 0) {
            ticketTemplate = data.map(function(item: any, index: any) {
                return (
                    <div key={item.id}>
                        <Article data={item} />
                    </div>
                )
            })
        } else {
            ticketTemplate = <p>К сожалению, квитанций нет</p>
        }

        return (
            <div className="ticket">
                <strong className={`ticket__count $(data.length > 0 ? '':'none')` }>Всего квитанций: {data.length}</strong>
                {ticketTemplate}
            </div>
        );
    }
}

export class App extends React.Component<{}, {}> {
    render() {
        return (
            <div className="wrapper">
                <div className="app">
                    <h2>Список квитанций</h2>
                    <Ticket data={my_tickets} />
                </div>
            </div>
        );
    }
}

interface IArticle {
    id: string;
    text: string;
    organization: string;
}

interface IArticleProps {
    data: IArticle;
}

interface IArticleState {
}

export class Article extends React.Component<IArticleProps, IArticleState> {
    render() {
        const {text, organization} = this.props.data;

        return(
            <span className="article_wrapper">
                <div className='article'>
                    <span className='ticket__organization'>{organization}:</span>
                    <span className='ticket__text'>{text}</span>
                </div>
                <span className = "wrap_btn">
                    <button className = "main_btn" > View </button>
                    <button className = "main_btn" > Edit </button>
                    <button className = "main_btn" > Del  </button>
                </span>
            </span>
        );
    }
}


export class Main extends React.Component<{}, {}> {
    render() {
        return(
            <App />
        )
    }
}